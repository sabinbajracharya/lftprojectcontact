package leapforg.contact.contactlftapp.presentation.presenters.base;

/**
 * Created by Sabin on 7/30/2017.
 */

public interface BasePresenter {
    void resume();
    void pause();
    void stop();
    void destroy();
    void onError(String message);
}
