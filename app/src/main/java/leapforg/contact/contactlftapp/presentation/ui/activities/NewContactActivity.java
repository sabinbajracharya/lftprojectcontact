package leapforg.contact.contactlftapp.presentation.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import leapforg.contact.contactlftapp.R;
import leapforg.contact.contactlftapp.domain.executor.impl.ThreadExecutor;
import leapforg.contact.contactlftapp.domain.model.ContactModel;
import leapforg.contact.contactlftapp.presentation.presenters.NewContactPresenter;
import leapforg.contact.contactlftapp.presentation.presenters.impl.NewContactPresenterImpl;
import leapforg.contact.contactlftapp.threading.MainThreadImpl;

public class NewContactActivity extends AppCompatActivity implements NewContactPresenter.View{

    private static final int RC_PHOTO_PICKER = 1;

    NewContactPresenterImpl mPresenter;

    Toolbar toolbar;

    EditText name, email, mobile, address;
    ImageButton mPhotoPickerButton;

    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_contact);
        mPresenter = new NewContactPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        address = (EditText) findViewById(R.id.address);
        mobile = (EditText) findViewById(R.id.mobile);
        mPhotoPickerButton = (ImageButton) findViewById(R.id.photoPickerButton);

        mPhotoPickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/jpeg");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(i, "Complete action using"), RC_PHOTO_PICKER);
            }
        });
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK) {
                imageUri = data.getData();
                mPhotoPickerButton.setImageURI(null);
                mPhotoPickerButton.setImageURI(imageUri);
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.save) {
            mPresenter.start(
                    new ContactModel(
                            name.getText().toString(),
                            "",
                            mobile.getText().toString(),
                            email.getText().toString(),
                            address.getText().toString(),
                            "http://pic.com"
                    ), imageUri
            );
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void exit() {
        finish();
    }
}
