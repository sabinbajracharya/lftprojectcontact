package leapforg.contact.contactlftapp.presentation.presenters.impl;

import android.net.Uri;
import android.support.annotation.Nullable;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import leapforg.contact.contactlftapp.domain.executor.Executor;
import leapforg.contact.contactlftapp.domain.executor.MainThread;
import leapforg.contact.contactlftapp.domain.interactors.NewContactInteractor;
import leapforg.contact.contactlftapp.domain.interactors.impl.NewContactInteractorImpl;
import leapforg.contact.contactlftapp.domain.model.ContactModel;
import leapforg.contact.contactlftapp.domain.repository.impl.Firebase;
import leapforg.contact.contactlftapp.presentation.presenters.NewContactPresenter;
import leapforg.contact.contactlftapp.presentation.presenters.base.AbstractPresenter;

/**
 * Created by Sabin on 7/30/2017.
 */

public class NewContactPresenterImpl extends AbstractPresenter implements NewContactPresenter, NewContactInteractorImpl.Callback {

    private View mView;
    private DatabaseReference mDatabaseRef;

    @Nullable
    StorageReference photoReference;
    NewContactInteractor interactor;
    ContactModel model;
    Uri image;

    public NewContactPresenterImpl(Executor executor,
                                   MainThread mainThread,
                                   View view) {
        super(executor, mainThread);
        mView = view;
        // initialize the interactor
        interactor = new NewContactInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                Firebase.getInstance()
        );
    }

    public void start(ContactModel model, Uri image) {
        this.model = model;
        this.image = image;
        interactor.setImageUri(image);
        interactor.execute();
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {
    }

    @Override
    public void onError(String message) {
    }

    @Override
    public void setDatabaseAndStorageRef(DatabaseReference dRef, StorageReference sRef) {
        this.mDatabaseRef = dRef;
        this.photoReference = sRef;
    }

    @Override
    public void save() {

        //upload file to firebase
        if(photoReference == null) {
            mDatabaseRef.push().setValue(model);
            mView.exit();
        } else {
            photoReference.putFile(image).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    model.setImage(downloadUrl.toString());
                    mDatabaseRef.push().setValue(model);
                    mView.exit();
                }
            });
        }
    }


}
