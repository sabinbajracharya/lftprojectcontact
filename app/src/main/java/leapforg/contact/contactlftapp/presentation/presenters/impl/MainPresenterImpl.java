package leapforg.contact.contactlftapp.presentation.presenters.impl;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import leapforg.contact.contactlftapp.domain.executor.Executor;
import leapforg.contact.contactlftapp.domain.executor.MainThread;
import leapforg.contact.contactlftapp.domain.interactors.SampleInteractor;
import leapforg.contact.contactlftapp.domain.interactors.impl.SampleInteractorImpl;
import leapforg.contact.contactlftapp.domain.model.ContactModel;
import leapforg.contact.contactlftapp.domain.repository.impl.Firebase;
import leapforg.contact.contactlftapp.presentation.presenters.MainPresenter;
import leapforg.contact.contactlftapp.presentation.presenters.base.AbstractPresenter;

/**
 * Created by Sabin on 7/30/2017.
 */

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter, SampleInteractor.Callback {

    private MainPresenter.View mView;
    private DatabaseReference mDatabaseRef;
    private ChildEventListener mChildEventListener;

    SampleInteractor interactor;

    public MainPresenterImpl(Executor executor,
                             MainThread mainThread,
                             View view) {
        super(executor, mainThread);
        mView = view;
        // initialize the interactor
        interactor = new SampleInteractorImpl(
                mExecutor,
                mMainThread,
                this,
                Firebase.getInstance()
        );
    }

    @Override
    public void resume() {

        // run the interactor
        mView.showProgress();
        interactor.execute();
    }

    @Override
    public void pause() {
        detachDatabaseReadListener();
        mView.clear();
    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void setDatabaseRef(DatabaseReference ref) {
        this.mDatabaseRef = ref;
    }

    protected void detachDatabaseReadListener() {

        if (mChildEventListener !=null) {
            mDatabaseRef.removeEventListener(mChildEventListener);
            mChildEventListener = null;
        }

    }

    @Override
    public void attachDatabaseReadListener() {
        mView.hideProgress();
        Log.d("ContactList", "attachDetabaseReadLIstener");
        if (mChildEventListener == null) {
            mChildEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ContactModel contact = dataSnapshot.getValue(ContactModel.class);
                    Log.d("ContactList", contact.getName());
                    mView.updateContactList(contact);
//                    mMessageAdapter.add(friendlyMessage);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };

            mDatabaseRef.addChildEventListener(mChildEventListener);
        }

    }
}
