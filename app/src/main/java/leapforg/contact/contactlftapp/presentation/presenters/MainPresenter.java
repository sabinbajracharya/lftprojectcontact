package leapforg.contact.contactlftapp.presentation.presenters;

import java.util.List;

import leapforg.contact.contactlftapp.domain.model.ContactModel;
import leapforg.contact.contactlftapp.presentation.presenters.base.BasePresenter;
import leapforg.contact.contactlftapp.presentation.ui.BaseView;

/**
 * Created by Sabin on 7/30/2017.
 */

public interface MainPresenter extends BasePresenter {

    interface View extends BaseView {
        // TODO: Add view methods
        void updateContactList(ContactModel contactList);
        void clear();
    }

    // TODO: Add other presenter methods
}
