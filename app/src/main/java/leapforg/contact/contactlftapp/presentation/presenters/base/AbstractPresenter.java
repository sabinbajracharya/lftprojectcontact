package leapforg.contact.contactlftapp.presentation.presenters.base;

import leapforg.contact.contactlftapp.domain.executor.Executor;
import leapforg.contact.contactlftapp.domain.executor.MainThread;

/**
 * Created by Sabin on 7/30/2017.
 * This is a base class for all presenters which are communicating with usecase. This base class will hold a
 * reference to the Executor and MainThread objects that are needed for running usecases in a background thread
 */

public abstract class AbstractPresenter {
    protected Executor mExecutor;
    protected MainThread mMainThread;

    public AbstractPresenter(Executor executor, MainThread mainThread) {
        mExecutor = executor;
        mMainThread = mainThread;
    }
}
