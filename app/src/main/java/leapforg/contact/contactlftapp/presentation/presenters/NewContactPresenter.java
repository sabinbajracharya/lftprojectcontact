package leapforg.contact.contactlftapp.presentation.presenters;

import leapforg.contact.contactlftapp.domain.model.ContactModel;
import leapforg.contact.contactlftapp.presentation.presenters.base.BasePresenter;
import leapforg.contact.contactlftapp.presentation.ui.BaseView;

/**
 * Created by Sabin on 7/30/2017.
 */

public interface NewContactPresenter extends BasePresenter {

    interface View extends BaseView {
        // TODO: Add view methods
        void exit();
    }

    // TODO: Add other presenter methods
}
