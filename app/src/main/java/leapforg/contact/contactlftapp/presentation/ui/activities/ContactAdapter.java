package leapforg.contact.contactlftapp.presentation.ui.activities;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import leapforg.contact.contactlftapp.R;
import leapforg.contact.contactlftapp.domain.model.ContactModel;

/**
 * Created by Sabin on 7/30/2017.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    Context mContext;
    private List<ContactModel> contactList;

    public ContactAdapter(Context mContext, List<ContactModel> contactList) {
        this.mContext = mContext;
        this.contactList = contactList;
    }

    public void add(ContactModel contact) {
        contactList.add(contact);
        notifyItemInserted(contactList.size() - 1);
    }

    public void clear() {
        contactList.clear();
        notifyDataSetChanged();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_row, parent, false);
        return new ContactViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        ContactModel contact = contactList.get(position);
        holder.name.setText(contact.getName());
        holder.email.setText(contact.getEmail());
        if(!contact.getImage().equals("")) {
            Picasso.with(mContext).load(contact.getImage()).resize(42, 42).into(holder.contactImage);
        }
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        public TextView name, email;
        ImageView contactImage;

        public ContactViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            email = (TextView) view.findViewById(R.id.email);
            contactImage = (ImageView) view.findViewById(R.id.contact_image);
        }
    }
}
