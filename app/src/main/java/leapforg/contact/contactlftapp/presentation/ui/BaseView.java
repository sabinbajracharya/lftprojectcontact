package leapforg.contact.contactlftapp.presentation.ui;

/**
 * Created by Sabin on 7/30/2017.
 * All views should implements this methods.
 */

public interface BaseView {
    void showProgress();
    void hideProgress();
    void showError(String message);
}
