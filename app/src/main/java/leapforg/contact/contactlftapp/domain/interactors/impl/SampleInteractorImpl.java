package leapforg.contact.contactlftapp.domain.interactors.impl;

import com.google.firebase.database.DatabaseReference;

import leapforg.contact.contactlftapp.domain.executor.Executor;
import leapforg.contact.contactlftapp.domain.executor.MainThread;
import leapforg.contact.contactlftapp.domain.interactors.SampleInteractor;
import leapforg.contact.contactlftapp.domain.interactors.base.AbstractInteractor;
import leapforg.contact.contactlftapp.domain.repository.Repository;

/**
 * Created by Sabin on 7/30/2017.
 */

public class SampleInteractorImpl extends AbstractInteractor implements SampleInteractor {

    private SampleInteractor.Callback mCallback;
    private Repository mRepository;

    public SampleInteractorImpl(Executor threadExecutor,
                                MainThread mainThread,
                                Callback callback, Repository repository) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mRepository = repository;
    }

    private void postDatabase(final DatabaseReference ref) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.setDatabaseRef(ref);
            }
        });
    }

    private void postDatabaseReadListener() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.attachDatabaseReadListener();
            }
        });
    }

    private void postMessage(final String msg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {

            }
        });
    }


    @Override
    public void run() {
        // TODO: Implement this with your business logic
        DatabaseReference mFirebaseDBRef = mRepository.getFirebaseDatabase("contact");
        postDatabase(mFirebaseDBRef);
        postDatabaseReadListener();
    }
}
