package leapforg.contact.contactlftapp.domain.repository;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;

import leapforg.contact.contactlftapp.domain.model.ContactModel;

/**
 * Created by Sabin on 7/30/2017.
 */

public interface Repository {

    boolean insert(ContactModel model);

    boolean update(ContactModel model);

    DatabaseReference getFirebaseDatabase(String name);
    StorageReference getFirebaseStorage(String Uri);

    boolean delete(ContactModel model);
}
