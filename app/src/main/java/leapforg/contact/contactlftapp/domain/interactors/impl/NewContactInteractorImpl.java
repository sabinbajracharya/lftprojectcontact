package leapforg.contact.contactlftapp.domain.interactors.impl;

import android.net.Uri;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;

import leapforg.contact.contactlftapp.domain.executor.Executor;
import leapforg.contact.contactlftapp.domain.executor.MainThread;
import leapforg.contact.contactlftapp.domain.interactors.NewContactInteractor;
import leapforg.contact.contactlftapp.domain.interactors.base.AbstractInteractor;
import leapforg.contact.contactlftapp.domain.repository.Repository;

/**
 * Created by Sabin on 7/30/2017.
 */

public class NewContactInteractorImpl extends AbstractInteractor implements NewContactInteractor {

    private Callback mCallback;
    private Repository mRepository;

    Uri imageUri;

    public NewContactInteractorImpl(Executor threadExecutor,
                                    MainThread mainThread,
                                    Callback callback, Repository repository) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mRepository = repository;
    }

    private void postReference(final DatabaseReference dRef, final StorageReference sRef) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.setDatabaseAndStorageRef(dRef, sRef);
            }
        });
    }

    private void postSave() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.save();
            }
        });
    }

    @Override
    public void run() {
        // TODO: Implement this with your business logic
        DatabaseReference mFirebaseDBRef = mRepository.getFirebaseDatabase("contact");
        StorageReference mPhotoRef = mRepository.getFirebaseStorage("contact_photos");

        StorageReference photoReference = null;
        if (imageUri != null) {
            photoReference = mPhotoRef.child(imageUri.getLastPathSegment());
        }
        postReference(mFirebaseDBRef, photoReference);
        postSave();
    }

    @Override
    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }
}
