package leapforg.contact.contactlftapp.domain.interactors.base;

/**
 * Created by Sabin on 7/30/2017.
 */

public interface Interactor {

    /**
     * This is the main method that starts an interactor.
     * It will make sure that the interactor operation is done on a background thread.
     */
    void execute();
}
