package leapforg.contact.contactlftapp.domain.interactors;

import android.net.Uri;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;

import leapforg.contact.contactlftapp.domain.interactors.base.Interactor;

/**
 * Created by Sabin on 7/30/2017.
 */

public interface NewContactInteractor extends Interactor {

    interface Callback {
        // TODO: Add interactor callback methods here
        void setDatabaseAndStorageRef(DatabaseReference dRef, StorageReference sRef);
        void save();
    }

    // TODO: Add interactor methods here
    void setImageUri(Uri imageUri);
}
