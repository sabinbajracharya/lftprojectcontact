package leapforg.contact.contactlftapp.domain.executor;

/**
 * Created by Sabin on 7/30/2017.
 */

public interface MainThread {

    /**
     * Make runnable operation run in the main thread.
     *
     * @param runnable The runnable to run.
     */
    void post(final Runnable runnable);
}
