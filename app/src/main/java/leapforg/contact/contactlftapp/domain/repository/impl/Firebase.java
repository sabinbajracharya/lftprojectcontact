package leapforg.contact.contactlftapp.domain.repository.impl;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import leapforg.contact.contactlftapp.domain.model.ContactModel;
import leapforg.contact.contactlftapp.domain.repository.Repository;

/**
 * Created by Sabin on 7/30/2017.
 */

public class Firebase implements Repository{

    private static Firebase mFirebase;

    private static FirebaseDatabase database;
    private static FirebaseStorage storage;

    public static Firebase getInstance() {
        if (mFirebase == null) {
            database = FirebaseDatabase.getInstance();
            storage = FirebaseStorage.getInstance();
            mFirebase = new Firebase();
        }

        return mFirebase;
    }

    @Override
    public boolean insert(ContactModel model) {
        return false;
    }

    @Override
    public boolean update(ContactModel model) {
        return false;
    }

    @Override
    public DatabaseReference getFirebaseDatabase(String name) {
        return database.getReference().child(name);
    }

    @Override
    public StorageReference getFirebaseStorage(String name) {
        return storage.getReference().child(name);
    }


    @Override
    public boolean delete(ContactModel model) {
        return false;
    }
}
