package leapforg.contact.contactlftapp.domain.executor;

import leapforg.contact.contactlftapp.domain.interactors.base.AbstractInteractor;

/**
 * Created by Sabin on 7/30/2017.
 */

public interface Executor {

    /**
     * This method should call the interactor's run method and thus start the interactor. This should be called
     * on a background thread as interactors might do lengthy operations.
     *
     * @param interactor The interactor to run.
     */
    void execute(final AbstractInteractor interactor);
}
