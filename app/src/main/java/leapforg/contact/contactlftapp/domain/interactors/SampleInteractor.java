package leapforg.contact.contactlftapp.domain.interactors;

import com.google.firebase.database.DatabaseReference;

import leapforg.contact.contactlftapp.domain.interactors.base.Interactor;

/**
 * Created by Sabin on 7/30/2017.
 */

public interface SampleInteractor extends Interactor {

    interface Callback {
        // TODO: Add interactor callback methods here
        void setDatabaseRef(DatabaseReference ref);
        void attachDatabaseReadListener();
    }

    // TODO: Add interactor methods here
}
