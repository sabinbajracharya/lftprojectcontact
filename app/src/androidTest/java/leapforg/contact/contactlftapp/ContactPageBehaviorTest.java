package leapforg.contact.contactlftapp;

import android.content.ComponentName;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import leapforg.contact.contactlftapp.presentation.ui.activities.MainActivity;
import leapforg.contact.contactlftapp.presentation.ui.activities.NewContactActivity;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Sabin on 7/31/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest


public class ContactPageBehaviorTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<MainActivity>(
            MainActivity.class);

    @Test
    public void openNewContactActivityAndWriteData() {
        Intents.init();
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withContentDescription(R.string.new_contact)).perform(click());
        intended(hasComponent(new ComponentName(getTargetContext(), NewContactActivity.class)));

        onView(withId(R.id.name)).perform(typeText("Sabin Bajracharya"));
        onView(withId(R.id.email)).perform(typeText("sabin@gmail.com"));
        onView(withId(R.id.mobile)).perform(typeText("9840456284"));
        onView(withId(R.id.address)).perform(typeText("Kathmandu, Nepal"), closeSoftKeyboard());

        onView(withId(R.id.name)).check(matches(withText("Sabin Bajracharya")));
        onView(withId(R.id.email)).check(matches(withText("sabin@gmail.com")));
        onView(withId(R.id.mobile)).check(matches(withText("9840456284")));
        onView(withId(R.id.address)).check(matches(withText("Kathmandu, Nepal")));
        Intents.release();
    }
}
