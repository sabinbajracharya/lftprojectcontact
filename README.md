# README #

Leapfrog Technology Contact App

### What is this repository for? ###

* Contact info saving application.
* Version 0.1

### What does the project contains? ###

* Firebase Database and Storage
* Clean Architecture
* Simple Espresso test

### Leftout ###

* Did not use authentication
* Offline contact storage and upload when internet availability is missing :(